package tracePetApp;

import controller.LocatorController;
import tracepet.TracePetInitializer;
import tracepet.TracePetObservable;
import view.TracePetView;

public class TracePetMain {
    public static void main(String[] args) {
        String rutaArchivo = "resources/trace_pet.json";
        TracePetObservable model = TracePetInitializer.buildTracePetObservable(rutaArchivo);
        LocatorController controller = new LocatorController(model);
        TracePetView locatorView = new TracePetView(controller);
        model.subscribe(locatorView);
    }
}
