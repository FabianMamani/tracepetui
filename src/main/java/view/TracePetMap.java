package view;

import coordenates.Coordenates;
import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import java.awt.*;

public class TracePetMap extends JMapViewer implements Map{
    private MapMarker marker;

    public TracePetMap() {
        super();
        setDisplayPosition(new Coordinate(-34.5226782, -58.7004231), 17);
    }

    @Override
    public void showPetPosition(String petName, Coordenates coordenates, int zoom) {
        Coordinate c = toCoordinate(coordenates);
        moveMap(c, zoom);
        drawPetPosition(petName, c);
    }

    private void moveMap(Coordinate coordinate, int zoom) { setDisplayPosition(coordinate, zoom); }

    private void drawPetPosition(String petName, Coordinate coordinate) {
        marker = null;
        marker = new MapMarkerDot(petName, coordinate);
        marker.getStyle().setBackColor(Color.RED);
        addMapMarker(marker);
    }

    // Castea la coordenada del core a la coordenada de JMapViewer.
    private Coordinate toCoordinate(Coordenates coordenates) {
        return new Coordinate(coordenates.getLatitude(), coordenates.getLongitude());
    }
}
