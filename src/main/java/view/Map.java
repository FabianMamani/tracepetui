package view;

import coordenates.Coordenates;

public interface Map {

    void showPetPosition(String petName, Coordenates coord, int zoom);

}
