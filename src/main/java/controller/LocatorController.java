package controller;

import channelNotification.MainNotificator;
import tracepet.TracePetObservable;
import breaker.NotifierCircuitBreaker;
import view.TracePetView;

import javax.swing.*;

public class LocatorController {

    private TracePetObservable tracePetObservable;

    public LocatorController(TracePetObservable tracePetObservable){
        this.tracePetObservable = tracePetObservable;
    }

    public void locate(String petName){
        this.tracePetObservable.locatePet(petName);
    }

    public void checkRetryAttempts(){
        this.tracePetObservable.getNotifierCircuitBreaker().initCircuitBreaker(0,5000,3);
    }

    public void sendNotification(String channel){
        checkRetryAttempts();
        this.tracePetObservable.getNotificator().setChannel(channel);
        if(this.tracePetObservable.getNotifierCircuitBreaker().getFinalStatus().equals("CLOSED")){
            System.out.println(this.tracePetObservable.getNotificator().sendAlert());
        }
        else
            System.out.println("Falló la notificación el servicio no responde");
    }

    public void loadNotificatorChannels(TracePetView view){
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        if(this.tracePetObservable.getNotificator().countChannels() > 0){
            //defaultComboBoxModel1.addAll(this.tracePetObservable.getNotificator());
            view.get_cmBxNotification().setModel(defaultComboBoxModel1);
        }
    }
}
